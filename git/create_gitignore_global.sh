#!/bin/bash
################################################################################
# File : create_gitignore_global.sh
#
# Usage: ./create_gitignore_global.sh
#
# Description: Make your life easier with a global .gitignore.
#              Operating system files should go in a global gitignore.
#              This might include things like your IDEs settings, or temp files,
#              metadata files, icon cache files etc...
#
#              Compatible and tested with Fedora Workstation 32.
#
# Author:  Moisés Muñoz <mnmoises@gmail.com> <https://www.moisesmunoz.com>
# Created: 28-08-2020
# License: GNU General Public License v2 or later
################################################################################

# ------------------------------------------------------------------------------
# | VARIABLES                                                                  |
# ------------------------------------------------------------------------------
gitignore_global="${HOME}/.gitignore_global"

# On Windows:
# gitignore_global="C:\Users\{myusername}\.gitignore_global"

# ------------------------------------------------------------------------------
# | MAIN FUNCTIONS                                                             |
# ------------------------------------------------------------------------------

# Create a global .gitignore file.
createGlobalGitIgnore() {
  if [[ ! -f "${gitignore_global}" ]]; then
    touch "${gitignore_global}"
    echo -e "\e[1;32mSUCCESS:\e[0m The ${gitignore_global} file has been created."
  else
    echo -e "\e[1mINFO:\e[0m The ${gitignore_global} file already exists."
  fi
}

# Populate the .gitignore_global file
populateGlobalGitIgnore() {
  cat <<EOF > "${gitignore_global}"
# Compiled source #
###################
*.class
*.com
*.dll
*.exe
*.o
*.so
*.pyc
*.pyo


# IDE files #
#############
# PhpStorm
.idea/

.iml
.vscode
*.classpath

# Sublime Text
/*.sublime-project
*.sublime-workspace


# Logs and databases #
######################
*.log
*.sqlite


### Node/npm ###
.npm-debug.log
node_modules/


### Mac OSX ###
.AppleDouble
.DS_Store
.DS_Store?
.LSOverride
# Icon must end with two \r
Icon
# Thumbnails
._*
# Files that might appear on external disk
.com.apple.timemachine.donotpresent
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
# Directories potentially created on remote AFP share
.apdisk
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items

### Windows ###
# Windows image file caches
ehthumbs.db
ehthumbs_vista.db
Thumbs.db
Thumbs.db:encryptable
# Dump file
*.stackdump
# Folder config file
[Dd]esktop.ini
# Recycle Bin used on file shares
$RECYCLE.BIN/
# Windows shortcuts
*.lnk

### Linux ###
*~
# KDE directory preferences
.directory


# Packages #
############
# it's better to unpack these files and commit the raw source
# git has its own built in compression methods
*.7z
*.bz2
*.bzip
*.bzip2
*.cab
*.gz
*.gzip
*.jar
*.lzma
*.rar
*.tgz
*.xar
*.xz
*.zip

# Packing-only formats
*.iso
*.tar

# Package management formats
*.deb
*.dmg
*.egg
*.gem
*.msi
*.msix
*.msm
*.msp
*.rpm
*.txz
*.xpi


# Temporary files #
###################
*.bak
*.swp
*.swo
*~
*#
EOF
}


# ------------------------------------------------------------------------------
# | INITIALIZE PROGRAM                                                         |
# ------------------------------------------------------------------------------
main() {
  createGlobalGitIgnore
  populateGlobalGitIgnore

  # Configure Git to use the exclude file ~/.gitignore_global for all Git repositories.
  git config --global core.excludesfile "${gitignore_global}"

  # On Windows:
  # git config --global core.excludesfile "%USERPROFILE%\.gitignore_global"

  # Using Windows PowerShell:
  # git config --global core.excludesfile "$Env:USERPROFILE\.gitignore_global"
}

# Run main program
main

exit